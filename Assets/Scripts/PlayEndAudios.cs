﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayEndAudios : MonoBehaviour {

	void PlayAudioEnd1(){
		GetComponents<AudioSource>()[0].Play();
	}

	void PlayAudioEnd2(){
		GetComponents<AudioSource>()[1].Play();
	}
}
