﻿using DataLayer.Variables;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GameLogic.Utils
{
	/// <summary>
	/// This class loads an scene when an event is raised
	/// </summary>
	public class IntroController : MonoBehaviour
	{
		public Text TextComponent;

		/// <summary>
		/// When this event is raised the specified scene is loaded
		/// </summary>
		[Header("Variables to read")]
		[SerializeField]
		private DL_Void _eventToListen;

		/// <summary>
		/// Path to the scene to be loaded
		/// </summary>
		[HideInInspector] [SerializeField]
		private string _scenePath;

		public string[] ListOfTexts;
		private List<string> m_currentTexts = new List<string>();

		private bool m_sceneLoaded = false;
		private bool m_stopRegisteringKeys = false;

		// Use this for initialization
		void Awake()
		{
			m_currentTexts = new List<string>(ListOfTexts);
			_eventToListen.Subscribe(NextStep);
			NextStep();
		}

		private void OnDestroy()
		{
			_eventToListen.Unsubscribe(NextStep);
		}

		private void NextStep()
		{
			if(m_stopRegisteringKeys)
			{
				return;
			}
			if(m_currentTexts.Count > 0)
			{
				TextComponent.text = m_currentTexts[0];
				m_currentTexts.RemoveAt(0);
				m_stopRegisteringKeys = true;
				Invoke("ResetRegistering", 0.3f);
				return;
			}

			if(!m_sceneLoaded)
			{
				m_sceneLoaded = true;
				Invoke("LoadScene", 1.0f);
			}
			m_stopRegisteringKeys = true;
			Invoke("ResetRegistering", 0.3f);
		}

		private void Update()
		{
			if(m_sceneLoaded)
			{
				TextComponent.color = new Color(TextComponent.color.r, TextComponent.color.g, TextComponent.color.b, TextComponent.color.a - Time.deltaTime);
			}
		}

		private void LoadScene()
		{
			SceneManager.LoadScene(_scenePath, LoadSceneMode.Single);
		}

		private void ResetRegistering()
		{
			m_stopRegisteringKeys = false;
		}
	}
}
