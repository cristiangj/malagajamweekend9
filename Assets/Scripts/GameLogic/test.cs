﻿using System.Collections;
using System.Collections.Generic;
using DataLayer.Variables;
using UnityEngine;

public class test : MonoBehaviour {

	public DL_Float ejemplo;

	private float n;
	
	// Use this for initialization
	void Awake () {
		ejemplo.Subscribe(Change);
	}

	private void OnDestroy() {
		ejemplo.Unsubscribe(Change);
	}

	// Update is called once per frame
	void Update () {
		n--;
		ejemplo.SetValue(n);
		ejemplo.GetValue();
		
	}

	void Change() {
		
	}
}
