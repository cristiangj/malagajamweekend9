﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataLayer.Variables;

public class startFadeIn : MonoBehaviour{
	public DL_Bool dlActiveFadeIn;

	// Use this for initialization
	void Start () {
		dlActiveFadeIn.SetValue(true);
	}
	
}