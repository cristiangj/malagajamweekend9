﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMusic : MonoBehaviour
{
	private void Awake()
	{
		SceneManager.sceneLoaded += OnSceneLoaded;
	}

	private void OnSceneLoaded(Scene _scene, LoadSceneMode _mode)
	{
		if(_scene.name == "P0" || _scene.name == "Walk")
		{
			GetComponent<AudioSource>().Play();
		}
		else if(_scene.name == "MainMenu")
		{
			GetComponent<AudioSource>().Stop();
		}
	}

	private void OnDestroy()
	{
		SceneManager.sceneLoaded -= OnSceneLoaded;
	}
}
