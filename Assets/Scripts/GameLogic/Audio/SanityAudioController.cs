﻿using System.Collections;
using DataLayer.Variables;
using UnityEngine;

namespace GameLogic.Audio {
	public class SanityAudioController : MonoBehaviour {

		[SerializeField] 
		private AudioSource _panting1;
		[SerializeField] 
		private AudioSource _panting2;
		[SerializeField] 
		private AudioSource _panting3;
		[SerializeField]
		private AudioSource _scream;
		[SerializeField]
		private float _minPitch;
		[SerializeField]
		private float _maxPitch;
		
		[Header("Variables")]
		[SerializeField]
		private DL_Float _sanity;

	
		// Use this for initialization
		void Awake () {
			_sanity.Subscribe(OnSanityChanged);
		}
	
		// Update is called once per frame
		private void OnDestroy() {
			_sanity.Unsubscribe(OnSanityChanged);
		}

		private void OnSanityChanged() {

			float sanity = _sanity.GetValue();
			float randomPitch = Random.Range(_minPitch, _maxPitch);

			_panting1.Stop();
			_panting2.Stop();
			_panting3.Stop();
			
			if (sanity == 3) {
				_panting1.pitch = randomPitch;
				_panting1.Play();
			} else if (sanity == 2) {
				_panting2.pitch = randomPitch;
				_panting2.Play();
			} else if (sanity == 1) {
				_panting3.pitch = randomPitch;
				_panting3.Play();
			} else if (sanity == 0) {
				_scream.pitch = randomPitch;
				_scream.Play();
			}
		}
	}
}
