﻿using System.Collections;
using DataLayer.Variables;
using UnityEngine;

namespace GameLogic.Audio {
	public class LaughAudioController : MonoBehaviour {

		[SerializeField] 
		private AudioSource _laugh1;
		[SerializeField] 
		private AudioSource _laugh2;
		[SerializeField]
		private float _minPitch;
		[SerializeField]
		private float _maxPitch;
		
		[Header("Variables")]
		[SerializeField]
		private DL_Bool _spectrumVisible;

	
		// Use this for initialization
		void Awake () {
			_spectrumVisible.Subscribe(OnSpectrumAppear);
		}
	
		// Update is called once per frame
		private void OnDestroy() {
			_spectrumVisible.Unsubscribe(OnSpectrumAppear);
		}

		private void OnSpectrumAppear() {

			if (_spectrumVisible.GetValue()) {
				_laugh1.Stop();
				_laugh2.Stop();

				RandomizeLaugh();	
			}
		}

		private void RandomizeLaugh() {
			int randomAudio = Random.Range(0, 2);
			float randomPitch = Random.Range(_minPitch, _maxPitch);

			if (randomAudio == 0) {
				_laugh1.pitch = randomPitch;
				_laugh1.Play();
			} else {
				_laugh2.pitch = randomPitch;
				_laugh2.Play();	
			}
		}
	}
}
