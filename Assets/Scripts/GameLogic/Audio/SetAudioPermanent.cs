﻿using UnityEngine;

public class SetAudioPermanent : MonoBehaviour
{
	void Awake ()
	{
		SetAudioPermanent[] audios = FindObjectsOfType<SetAudioPermanent>();
		if(audios.Length > 1)
		{
			Destroy(gameObject);
			return;
		}
		DontDestroyOnLoad(gameObject);
	}
}
