﻿using System.Collections;
using DataLayer.Variables;
using UnityEngine;

namespace GameLogic.Audio {
	public class BackgroundAudioController : MonoBehaviour {

		[SerializeField] 
		private AudioSource _music;
		[SerializeField]
		[Range(0f,1f)] private float _musicMinVolume; 
		[SerializeField]
		private AudioSource _rain;
		[SerializeField]
		[Range(0f,1f)] private float _rainMinVolume; 
		[SerializeField]
		private AudioSource _thunder1;
		[SerializeField]
		[Range(0f,1f)] private float _thunder1MinVolume; 
		[SerializeField]
		private AudioSource _thunder2;
		[SerializeField]
		[Range(0f,1f)] private float _thunder2MinVolume; 
		[SerializeField]
		[Range(0f,100f)] private float _gradientVelocity;
	
		[Header("Variables")]
		[SerializeField]
		private DL_Bool _spectrumVisible;

	
		// Use this for initialization
		void Awake () {
			_spectrumVisible.Subscribe(OnSpectrumAppear);
		}
	
		// Update is called once per frame
		private void OnDestroy() {
			_spectrumVisible.Unsubscribe(OnSpectrumAppear);
		}

		private void OnSpectrumAppear() {
		
			StopAllCoroutines();
		
			if (_spectrumVisible.GetValue()) {
				StartCoroutine(BackgroundAudioFadeIn());	
			} else {
				StartCoroutine(BackgroundAudioFadeOut());
			}
		}

		private IEnumerator BackgroundAudioFadeIn() {	
			while (_music.volume > _musicMinVolume) {
				_music.volume = Mathf.Lerp(_music.volume, _musicMinVolume, Time.deltaTime * _gradientVelocity);
				_rain.volume = Mathf.Lerp(_rain.volume, _rainMinVolume, Time.deltaTime * _gradientVelocity);
				_thunder1.volume = Mathf.Lerp(_thunder1.volume, _thunder1MinVolume, Time.deltaTime * _gradientVelocity);
				_thunder2.volume = Mathf.Lerp(_thunder2.volume, _thunder1MinVolume, Time.deltaTime * _gradientVelocity);
			
				yield return null;
			}
		}
	
		private IEnumerator BackgroundAudioFadeOut() {
			while (_music.volume < 0.99f) {
				_music.volume = Mathf.Lerp(_music.volume, 1f, Time.deltaTime * _gradientVelocity);
				_rain.volume = Mathf.Lerp(_rain.volume, 1f, Time.deltaTime * _gradientVelocity);
				_thunder1.volume = Mathf.Lerp(_thunder1.volume, 1f, Time.deltaTime * _gradientVelocity);
				_thunder2.volume = Mathf.Lerp(_thunder2.volume, 1f, Time.deltaTime * _gradientVelocity);
			
				yield return null;
			}
		}
	}
}
