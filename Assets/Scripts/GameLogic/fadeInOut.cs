﻿using UnityEngine;
using UnityEngine.UI;
using DataLayer.Variables;

public class fadeInOut : MonoBehaviour{
	private float seconds;
   	private float secondsCounter=0;
	   
	private bool activeFadeIn;
	private bool activeFadeOut;

	public DL_Bool dlActiveFadeIn;
	public DL_Bool dlactiveFadeOut;
	
    public Image m_Image;
	public float time;



	void Awake () {
		dlActiveFadeIn.Subscribe(ChangeIn);
		dlactiveFadeOut.Subscribe(ChangeOut);
	}

	private void OnDestroy() {
		dlActiveFadeIn.Unsubscribe(ChangeIn);
		dlactiveFadeOut.Unsubscribe(ChangeOut);
	}

	void Start(){
		activeFadeIn = dlActiveFadeIn.GetValue();
		activeFadeOut = dlactiveFadeOut.GetValue();
	}

   	void Update(){
		Fade(activeFadeIn, dlActiveFadeIn, 0);
		Fade(activeFadeOut, dlactiveFadeOut, 1);
 	}

	void ChangeIn() {
		activeFadeIn = dlActiveFadeIn.GetValue();
	}

	void ChangeOut() {
		activeFadeOut = dlactiveFadeOut.GetValue();
	}

	void Fade(bool active, DL_Bool dlActive, float alpha){
	//	active = dlActive.GetValue();
	 	if (active == true){
            m_Image.CrossFadeAlpha(alpha, time * Time.deltaTime, false);
			seconds = time * Time.deltaTime;
			secondsCounter += Time.deltaTime;
			if(secondsCounter > seconds){
				Debug.Log(secondsCounter + " " + seconds);
				secondsCounter = 0;
				dlActive.SetValue(false);
				active = false;
			}
		}
	}

}