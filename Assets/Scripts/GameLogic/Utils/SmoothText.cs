﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using DataLayer.Variables;
using UnityEngine;
using UnityEngine.UI;

namespace Utils {
	public class SmoothText : MonoBehaviour {
		[Serializable]
		private class CharactersTime {
			public char character;
			public float time;
		}

		[Header("Parameters")] [SerializeField]
		private List<CharactersTime> _charactersTimes;

		[SerializeField] private float _defaultTime;
		private bool _haveToWait;

		/// <summary>
		/// Flag that indicates if the text typing is in progress
		/// </summary>
		[HideInInspector] public bool isPlaying;

		/// <summary>
		/// Event raised when finish complete the text suddenly is requested
		/// </summary>
		[Header("Variables to read")] [SerializeField]
		private DL_Void _completeTextTypingSuddenly;

		/// <summary>
		/// Event raised when start/stop typing text is requested
		/// </summary>
		[SerializeField] private DL_Bool _setEnabledTyping;

		/// <summary>
		/// Complete text to write in screen
		/// </summary>
		[SerializeField] private DL_String _textToWrite;

		/// <summary>
		/// Event raised when all the text is typed
		/// </summary>
		[Header("Variables to set")] [SerializeField]
		private DL_Void _finishedTextTyping;
		[SerializeField]
		private DL_Void _letterSoundTrigger;
		[SerializeField]
		private DL_Void _skipLettersTrigger;

		/// <summary>
		/// Smooth text to show in screen
		/// </summary>
		[SerializeField] private DL_String _textToShow;
		[SerializeField] private DL_Void _showIconTrigger;

		//References and cache variables
		private string _stringText;
		private int _numberOfImagesShown;

		private bool _textFinished;

		//private AudioSource letterAudio;

		private Coroutine _typeCoroutine;

		private void Awake() {
			//_textToWrite = GetComponent<Text>();

			//letterAudio = GetComponent<AudioSource>();
			//letterAudio.volume = 0.2f;
			_textFinished = false;
			_numberOfImagesShown = 0;
		}

		private void OnEnable() {
			//subscriptions
			_completeTextTypingSuddenly.Subscribe(OnCompleteTypingTextSuddenly);
			_setEnabledTyping.Subscribe(OnSetEnabledTextTyping);
		}

		private void OnDisable() {
			if (isPlaying) {
				OnCompleteTypingTextSuddenly();
			}

			//unsubscriptions
			_completeTextTypingSuddenly.Unsubscribe(OnCompleteTypingTextSuddenly);
			_setEnabledTyping.Unsubscribe(OnSetEnabledTextTyping);
		}

		public void Initialize() {
			_textToShow.SetValue(string.Empty);
		}

		public void Play() {
			if (_typeCoroutine != null) {
				StopCoroutine(_typeCoroutine);
			}

			Initialize();
			_typeCoroutine = StartCoroutine(TypeText());
		}

		private void Stop() {
			if (_typeCoroutine != null) {
				StopCoroutine(_typeCoroutine);
			}
		}

		private IEnumerator TypeText() {
			StringBuilder sb = new StringBuilder();

			isPlaying = true;
			_haveToWait = true;
			_textFinished = false;
			_numberOfImagesShown = 0;

			foreach (char character in _textToWrite.GetValue()) {
				if(character.ToString().Equals("&")){
					_showIconTrigger.Trigger();
					_numberOfImagesShown++;
				} else if(!character.Equals('*')){
					sb.Append(character);
					_textToShow.SetValue(sb.ToString());
				}

				if(!character.ToString().Equals(" ") && !character.ToString().Equals("\n") && !character.ToString().Equals("\r") && !character.Equals('*'))
					_letterSoundTrigger.Trigger();

				if(!character.ToString().Equals("\n"))
					yield return new WaitForSecondsRealtime(HowMuchToWait(character));
				else
					yield return null;
			}

			isPlaying = false;

			_finishedTextTyping.Trigger();
		}

		private float HowMuchToWait(char character) {
			if (!_haveToWait) {
				return 0f;
			}

			CharactersTime ct = _charactersTimes.Find(x => x.character.Equals(character));

			if (ct != null) {
				return ct.time;
			}

			return _defaultTime;
		}

#region Listeners

		private void OnSetEnabledTextTyping() {
			if (_setEnabledTyping.GetValue()) {
				Play();				
			} else {
				Stop();
			}
		}

		private void OnCompleteTypingTextSuddenly() {
			if (!_textFinished) {
				_textFinished = true;
				if (_typeCoroutine != null) {
					StopCoroutine(_typeCoroutine);	
				}

				int numberOfIcons = _textToWrite.GetValue().Split('&').Length - 1;

				for(int i = _numberOfImagesShown; i < numberOfIcons; i++) {
					_showIconTrigger.Trigger();
				}

				_textToShow.SetValue(_textToWrite.GetValue().Replace("&", ""));
				_textToShow.SetValue(_textToShow.GetValue().Replace("*", ""));
				isPlaying = false;
				_haveToWait = false;
				_skipLettersTrigger.Trigger();
				_finishedTextTyping.Trigger();
			}
		}

#endregion
	}
}