﻿using System.Collections;
using System.Collections.Generic;
using DataLayer.Variables;
using UnityEngine;

public class AnyKey : MonoBehaviour {
	
	[Header("Variables")]
	[SerializeField]
	private DL_Void _eventOnKey;
	
	// Update is called once per frame
	void Update () {
		if (Input.anyKey) {
			_eventOnKey.Trigger();
		}
	}
}
