﻿using DataLayer.Variables;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameLogic.Utils {
	/// <summary>
	/// This class loads an scene when an event is raised
	/// </summary>
	public class SceneLoader : MonoBehaviour {

		/// <summary>
		/// When this event is raised the specified scene is loaded
		/// </summary>
		[Header("Variables to read")]
		[SerializeField]
		private DL_Void _eventToListen;
		/// <summary>
		/// Path to the scene to be loaded
		/// </summary>
		[HideInInspector][SerializeField]
		private string _scenePath;
	
		// Use this for initialization
		void Awake () {
			_eventToListen.Subscribe(LoadScene);
		}

		private void OnDestroy() {
			_eventToListen.Unsubscribe(LoadScene);
		}

		private void LoadScene() {
			SceneManager.LoadScene(_scenePath, LoadSceneMode.Single);
		}
	}
}
