﻿#if UNITY_EDITOR 
	using UnityEditor;
	using UnityEditor.SceneManagement;
	
	namespace Utils {
		
		class LoadFirstScene : EditorWindow
		{
	 
			[MenuItem("Play/PlayMe _%h")]
			public static void RunMainScene()
			{
				EditorSceneManager.OpenScene("Assets/Data/Scenes/Loading.unity");
				EditorApplication.isPlaying = true;
			}
		}
	}
#endif
