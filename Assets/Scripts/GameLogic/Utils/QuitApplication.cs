﻿using DataLayer.Variables;
using UnityEngine;

namespace GameLogic.Utils {
	
	public class QuitApplication : MonoBehaviour {

		[SerializeField]
		private DL_Void _quitApplication;

		private void Awake() {
			_quitApplication.Subscribe(OnQuitApplication);
			transform.parent = null;
			DontDestroyOnLoad(this);
		}
		
		private void OnDestroy() {
			_quitApplication.Subscribe(OnQuitApplication);
		}

#region Listeners

		private void OnQuitApplication() {
			Debug.Log("Exiting for application...");
			Application.Quit();
		}

#endregion
	}
}
