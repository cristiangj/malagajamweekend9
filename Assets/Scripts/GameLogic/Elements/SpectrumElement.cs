﻿using System;
using DataLayer.Variables;
using GameLogic.Ghost;
using UnityEngine;

namespace GameLogic.Elements {
	
	public class SpectrumElement : ElementBase {
		
		[SerializeField] private DL_Void _playerScreamed;
		
		private GhostAnimationController m_ghostAnimationController;

		private void Awake()
		{
			m_ghostAnimationController = GetComponentInChildren<GhostAnimationController>();
		}

		private void OnDisable() {
			_isDiscovered = false;
		}

		public override void OnDiscover() {
			m_ghostAnimationController.Hide();
			_playerScreamed.Trigger();
			_sanityCount.SetValue(0);
		}
	}
}
