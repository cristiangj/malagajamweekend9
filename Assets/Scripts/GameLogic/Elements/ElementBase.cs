﻿using DataLayer.Variables;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public abstract class ElementBase : MonoBehaviour {
	
	[SerializeField] 
	protected float _amountOnEnter;
	[SerializeField] 
	protected bool _isDiscovered = false;
	
	[Header("Variables")]
	[SerializeField]
	protected DL_Float _sanityCount;
	
	public void SetAmountOnEnter(float amountOnEnter) {
		_amountOnEnter = amountOnEnter;
	}

	public void DiscoverElement() {
		_isDiscovered = true;
		OnDiscover();
	}

	public bool IsDiscovered() {
		return _isDiscovered;
	}
	
	public abstract void OnDiscover();
}
