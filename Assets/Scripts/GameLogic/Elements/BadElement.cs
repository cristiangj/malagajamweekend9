﻿namespace GameLogic.Elements {
	
	public class BadElement : ElementBase {
	
		public override void OnDiscover() {
			_sanityCount.SetValue(_sanityCount.GetValue() - _amountOnEnter);
		}
	}
}
