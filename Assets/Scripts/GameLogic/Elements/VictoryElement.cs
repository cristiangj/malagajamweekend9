﻿using DataLayer.Variables;
using UnityEngine;

namespace GameLogic.Elements {
	
	public class VictoryElement : ElementBase {

		[Header("Variables")]
		[SerializeField]
		private DL_Void _victoryEvent;
	
		public override void OnDiscover() {
			_victoryEvent.Trigger();
		}
	}
}
