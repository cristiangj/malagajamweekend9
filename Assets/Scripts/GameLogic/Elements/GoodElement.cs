﻿namespace GameLogic.Elements {
	
	public class GoodElement : ElementBase {
	
		public override void OnDiscover() {
			_sanityCount.SetValue(_sanityCount.GetValue() + _amountOnEnter);
		}
	}
}
