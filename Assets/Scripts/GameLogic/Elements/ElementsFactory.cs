﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameLogic.Elements {
	public class ElementsFactory : MonoBehaviour {

		[Serializable]
		private class ElementInstance {
			public string elementName;
			public float amountOnEnter;
			public ElementBase elementLogic;
			public Sprite elementVisual;
			public Vector2 coordinates;
		}

		[SerializeField] 
		private List<ElementInstance> _elements;
		
		// Use this for initialization
		private void Awake () {
			InitializeElements();
		}

		private void InitializeElements() {
			foreach (ElementInstance element in _elements) {
				GameObject gameObjectBase = new GameObject(element.elementName);
				
				Instantiate(element.elementLogic, gameObjectBase.transform).SetAmountOnEnter(element.amountOnEnter);
				
				GameObject gameObjectVisual = new GameObject("Visual");
				gameObjectVisual.AddComponent<SpriteRenderer>().sprite = element.elementVisual;
				gameObjectVisual.transform.parent = gameObjectBase.transform;	
				
				gameObjectBase.transform.position = element.coordinates;
			}
		}
	}
}
