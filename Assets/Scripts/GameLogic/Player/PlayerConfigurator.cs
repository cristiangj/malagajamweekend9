﻿using System.Collections;
using System.Collections.Generic;
using DataLayer.Variables;
using UnityEngine;

public class PlayerConfigurator : MonoBehaviour {

	[SerializeField]
	private float _maxSanity;
	
	[SerializeField]
	private DL_Float _sanity;
	
	// Use this for initialization
	void Awake () {
		_sanity.SetValue(_maxSanity);
	}
}
