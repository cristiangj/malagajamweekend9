﻿using DataLayer.Variables;
using UnityEngine;

namespace GameLogic.Player {
	public class View : MonoBehaviour {

		[SerializeField]
		private DL_Bool _inMovement;
		public DL_Vector2 directionPjDL;
		public float distance;
		public LayerMask layer;
	
		private Vector2 _position;
		private Vector2 _directionPj;
	

		private void Awake () {
			_inMovement.Subscribe(Change);
		}

		private void OnDestroy() {
			_inMovement.Unsubscribe(Change);
		}
	
		void Change() {
			_directionPj = directionPjDL.GetValue();
			_position = transform.position;

			//only on not in movement
			if (_inMovement.GetValue()) {
				return;
			}

			RaycastHit2D hit = Physics2D.Raycast(_position, _directionPj, distance, layer);

#if UNITY_EDITOR
			Debug.DrawRay(_position, _directionPj, Color.white, 0.3f); 	
#endif
			
			if (hit.collider != null){ // If it hits something...
				ElementBase element = hit.collider.gameObject.GetComponent<ElementBase>();

				if (!element.IsDiscovered()) {
					element.DiscoverElement();
				}
			}
		
		}

	}
}
