﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DataLayer.Variables;

public class sanityMeter : MonoBehaviour {

	public Slider sanitySlider;
	public DL_Float sanity;

	public float multiplier;

	public float maxSanity;

	private float sanityCount;

	void Awake () {
		sanity.Subscribe(Change);
	}

	private void OnDestroy() {
		sanity.Unsubscribe(Change);
	}

	// Use this for initialization
	void Start () {
		sanitySlider.maxValue = maxSanity;
		sanitySlider.value = maxSanity;
		sanity.SetValue(maxSanity);
		sanityCount = maxSanity;
		Change();
	}
	


	// Update is called once per frame
	void Update () {
		sanityCount = sanity.GetValue();

	 	if(sanityCount >= 0f){
			sanityCount -= multiplier * Time.deltaTime;
			sanity.SetValue(sanityCount);
		}
		
	}

	void Change() {
	 	sanityCount = sanity.GetValue();

		if(sanityCount <= maxSanity && sanityCount >= 0f){
			sanitySlider.value = sanityCount;
		}
	}
}
