﻿using DataLayer.Variables;
using UnityEngine;

namespace GameLogic.Player {
    
    public class Walk : MonoBehaviour
    {
        public float speed = 5;
        public string horizontal;
        public string vertical;
        public int tiles;
        public float collisionDistance = 0.5f;
        public LayerMask layers;

        [SerializeField]
        private float _coolDown = 0.2f;
        private float _currentTime = 0f;
        
        [SerializeField] 
        private float _minimumDistanceToMoveAgain = 0.05f;

        [SerializeField] 
        private string _wallTag;
     
        [Header("Variables")]
        [SerializeField]
        private DL_Pose _currentPosition;
        [SerializeField]
        private DL_Vector2 _directionPJ; //always between [-1,1] in x,y
        [SerializeField] 
        private DL_Bool _inMovement;
        [SerializeField]
        private DL_Void _onWallTouch;
        
        private Vector2 _axisValues = new Vector2();
        private bool _isInMovement = false;
        private Vector2 _direction;
        private Vector2 _initialPosition;
        private Vector2 _finalPosition;

        private Pose _currentPose = new Pose();

		private Vector3 m_initialPosition;
		private float m_lerp;
        
        private void Awake() {
            SetCurrentPose();
        }

        void Update() {
            Move();
        }

        private void Move() {

            _currentTime += Time.deltaTime;
            
            if (_isInMovement) {
                
                if (Vector2.Distance(transform.position, _finalPosition) < _minimumDistanceToMoveAgain) {
                    StopMovement();
                    CheckPlayerOnWall();
                }

				m_lerp += Time.deltaTime * speed;
				transform.position = Vector2.Lerp(m_initialPosition, _finalPosition, m_lerp);

                SetCurrentPose();
            }
            else {
                _axisValues.x = Input.GetAxisRaw(horizontal);
                _axisValues.y = Input.GetAxisRaw(vertical);

                _direction = Vector2.zero;

                if (_axisValues.x != 0 && _currentTime > _coolDown) {
                    _direction.x =  _axisValues.x;
                    _isInMovement = true;
					m_initialPosition = transform.position;
					m_lerp = 0;
					SetCurrentDirection();
					_currentTime = 0f;
				}
                else if (_axisValues.y != 0 && _currentTime > _coolDown) {
                    _direction.y = _axisValues.y;
                    _isInMovement = true;
					m_initialPosition = transform.position;
					m_lerp = 0;
					SetCurrentDirection();
					_currentTime = 0f;
				}
                
                if (CanMove()) {
                    _initialPosition = transform.position;
                    _finalPosition = _initialPosition + _direction * tiles;
                }
                else {
                    _isInMovement = false;
                }
                
                _inMovement.SetValue(_isInMovement);
            }

        }

        private void StopMovement() {
            _isInMovement = false;
            transform.position = _finalPosition;
            _inMovement.SetValue(_inMovement);
        }
        
        private void SetCurrentDirection() {
            _directionPJ.SetValue(_direction);
        }
        
        private void SetCurrentPose() {
            _currentPose.position = transform.position;
            _currentPosition.SetValue(_currentPose);
        }

        private void CheckPlayerOnWall() {
            if (isPlayerOnWall()) {
                _onWallTouch.Trigger();
            }
        }

        private RaycastHit2D CheckPlayerHit() {
            return Physics2D.Raycast(transform.position, _direction, collisionDistance, layers);
        }
        
        private bool CanMove() {
            RaycastHit2D hit = CheckPlayerHit();

            return hit.collider == null;
        }

        private bool isPlayerOnWall() {

            RaycastHit2D hit = CheckPlayerHit();

            return hit.collider != null && hit.collider.gameObject.CompareTag(_wallTag);
        }
       
    }
}

       

