﻿using System.Collections;
using System.Collections.Generic;
using DataLayer.Variables;
using UnityEngine;

public class LightController : MonoBehaviour {

	[SerializeField] 
	private SpriteRenderer _leftRight;
	[SerializeField] 
	private SpriteRenderer _UpDown;

	[Header("Variables")] 
	[SerializeField]
	private DL_Vector2 _directionPJ;
	
	// Use this for initialization
	private void Awake() {
		DisableAllLights();
		_UpDown.enabled = true;
		_UpDown.flipX = true;
		_leftRight.gameObject.SetActive(true);
		_UpDown.gameObject.SetActive(true);
		_directionPJ.Subscribe(OnPlayerMove);
	}

	private void OnDestroy() {
		_directionPJ.Unsubscribe(OnPlayerMove);
	}

	private void OnPlayerMove() {
		DisableAllLights();
		SetLightDirection();
	}

	private void SetLightDirection() {
		Vector2 direction = _directionPJ.value;

		if (direction.y > 0) {
			_UpDown.enabled = true;
			_UpDown.flipX = false;
		} else if (direction.x > 0) {
			_leftRight.enabled = true;
			_leftRight.flipX = true;
		} else if (direction.y < 0) {
			_UpDown.enabled = true;
			_UpDown.flipX = true;
		} else {
			_leftRight.enabled = true;
			_leftRight.flipX = false;
		}
	}

	private void DisableAllLights() {
		_UpDown.enabled = false;
		_leftRight.enabled = false;
	}
}
