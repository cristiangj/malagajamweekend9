﻿using DataLayer.Variables;
using UnityEngine;
using System.Collections;

namespace GameLogic.Player {
	public class Respawn : MonoBehaviour {

		public DL_Float sanity;

		public DL_Bool FadeIn;
		public DL_Bool FadeOut;

		[SerializeField] 
		private DL_Pose _currentPlayerPose;
		private Pose _pose = new Pose();
		
		public Vector2 positionRespawn;

		private float _maxSanityLevel;
		public float DelayToFadeout;
		public float DelayToRespawn;

		private Coroutine m_endCoroutine;

		void Awake () {
			sanity.Subscribe(Change);
		}

		private void Start() {
			_maxSanityLevel = sanity.GetValue();
		}

		private void OnDestroy() {
			sanity.Unsubscribe(Change);
		}

		void Change() {
			if(sanity.GetValue() <=  0f)
			{
				if(m_endCoroutine == null)
				{
					m_endCoroutine = StartCoroutine(EndCoroutine());
				}
			}
		}

		private IEnumerator EndCoroutine()
		{
			yield return new WaitForSeconds(DelayToFadeout);
			FadeIn.SetValue(true);
			yield return new WaitForSeconds(DelayToRespawn);
			RespawnPlayer(positionRespawn);
			sanity.SetValue(_maxSanityLevel);
			FadeOut.SetValue(false);
			m_endCoroutine = null;
			yield break;
		}

		void RespawnPlayer(Vector2 positionRespawn){
			transform.position = positionRespawn;
			_pose.position = positionRespawn;
			_currentPlayerPose.SetValue(_pose);
		}


	}
}
