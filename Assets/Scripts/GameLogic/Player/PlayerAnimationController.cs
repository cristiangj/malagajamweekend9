﻿using DataLayer.Variables;
using UnityEngine;
using Random = UnityEngine.Random;

namespace GameLogic.Player
{
	public enum AnimationDirection
	{
		Up,
		Down,
		Left,
		Right,
	}

	public enum AnimationType
	{
		Walk,
		Idle,
	}

	public class PlayerAnimationController : MonoBehaviour
	{
		public Animator FrontAnimator;
		public Animator BackAnimator;
		public Animator LateralAnimator;
		private Animator m_currentAnimator;

		public GameObject FrontMesh;
		public GameObject BackMesh;
		public GameObject LateralMesh;

		public float MinEyeTime = 1;
		public float MaxEyeTime = 5;

		public DL_Vector2 PlayerDirection;
		public DL_Bool PlayerIsMoving;
		[SerializeField] private DL_Float _sanity;
		
		private AnimationType m_currentType;
		private AnimationDirection m_currentDirection;

		private bool m_initialize;

		public AudioClip[] StepSoundList;
		private AudioSource m_audioSource;

		private void Awake()
		{
			UpdateAnimation(AnimationType.Idle, AnimationDirection.Down);
			Invoke("EyeLoop", Random.Range(MinEyeTime, MaxEyeTime));
			PlayerDirection.Subscribe(OnPlayerDirectionChange);
			PlayerIsMoving.Subscribe(OnPlayerMovingChange);
			_sanity.Subscribe(OnSanityLose);
			m_audioSource = gameObject.AddComponent<AudioSource>();
		}

		private void OnPlayerDirectionChange()
		{
			if(PlayerDirection.value.x != 0)
			{
				UpdateAnimation(m_currentType, PlayerDirection.value.x > 0 ? AnimationDirection.Right : AnimationDirection.Left);
			}
			else if(PlayerDirection.value.y != 0)
			{
				UpdateAnimation(m_currentType, PlayerDirection.value.y > 0 ? AnimationDirection.Up : AnimationDirection.Down);
			}
		}

		private void OnPlayerMovingChange()
		{
			if (PlayerIsMoving.value && m_currentType != AnimationType.Walk)
			{
				UpdateAnimation(AnimationType.Walk, m_currentDirection);
			}
			else if (!PlayerIsMoving.value && m_currentType != AnimationType.Idle)
				{
				UpdateAnimation(AnimationType.Idle, m_currentDirection);
			}
		}

		public void Clear()
		{
			FrontMesh.SetActive(false);
			BackMesh.SetActive(false);
			LateralMesh.SetActive(false);
			LateralMesh.transform.localScale = new Vector3(1, 1, 1);
			CancelInvoke("EyeLoop");
		}

		public void InitializeAnimation(AnimationType _type, AnimationDirection _direction)
		{
			UpdateDirection(_direction);
			UpdateType(_type);
			Invoke("EyeLoop", Random.Range(MinEyeTime, MaxEyeTime));
		}

		public void UpdateAnimation(AnimationType _type, AnimationDirection _direction)
		{
			UpdateDirection(_direction);
			UpdateType(_type);
		}

		private void UpdateDirection(AnimationDirection _direction)
		{
			m_currentDirection = _direction;
			switch (m_currentDirection)
			{
				case AnimationDirection.Up:
					FrontMesh.SetActive(false);
					BackMesh.SetActive(true);
					LateralMesh.SetActive(false);
					m_currentAnimator = BackAnimator;
					break;

				case AnimationDirection.Down:
					FrontMesh.SetActive(true);
					BackMesh.SetActive(false);
					LateralMesh.SetActive(false);
					m_currentAnimator = FrontAnimator;
					break;

				case AnimationDirection.Left:
					FrontMesh.SetActive(false);
					BackMesh.SetActive(false);
					LateralMesh.SetActive(true);
					m_currentAnimator = LateralAnimator;
					LateralMesh.transform.localScale = new Vector3(1, 1, 1);
					break;

				case AnimationDirection.Right:
					FrontMesh.SetActive(false);
					BackMesh.SetActive(false);
					LateralMesh.SetActive(true);
					m_currentAnimator = LateralAnimator;
					LateralMesh.transform.localScale = new Vector3(-1, 1, 1);
					break;
			}
		}

		private void UpdateType(AnimationType _type)
		{
			m_currentType = _type;
			switch (m_currentType)
			{
				case AnimationType.Walk:
					m_currentAnimator.CrossFade("Walk", 0.1f);
					break;

				case AnimationType.Idle:
					m_currentAnimator.CrossFade("Idle", 0.1f);
					break;
			}
		}

		private void OnSanityLose() {
			if (_sanity.GetValue() == 0) {
				PlayFear();
			}
		}
		
		public void PlayFear()
		{
			m_currentAnimator.Play("Fear");
		}

		private void CloseEyes()
		{
			if (m_currentAnimator != BackAnimator)
			{
				m_currentAnimator.Play("Close", 1);
			}
		}

		private void EyeLoop()
		{
			CloseEyes();
			Invoke("EyeLoop", Random.Range(MinEyeTime, MaxEyeTime));
		}

		private void OnDestroy()
		{
			CancelInvoke("EyeLoop");
			PlayerDirection.Unsubscribe(OnPlayerDirectionChange);
			PlayerIsMoving.Unsubscribe(OnPlayerMovingChange);
			_sanity.Unsubscribe(OnSanityLose);
		}

		public void Step()
		{
			m_audioSource.clip = StepSoundList[Random.Range(0, StepSoundList.Length)];
			m_audioSource.loop = false;
			m_audioSource.Play();
		}

		/*[ContextMenu("Init")]
		public void Init()
		{
			InitializeAnimation(AnimationType.Idle, AnimationDirection.Down);
		}

		[ContextMenu("Play Fear")]
		public void PlayFear1()
		{
			PlayFear();
		}

		[ContextMenu("Lateral Left")]
		public void LateralLeft()
		{
			InitializeAnimation(AnimationType.Idle, AnimationDirection.Left);
		}

		[ContextMenu("Lateral Left Walk")]
		public void LateralLeftWalk()
		{
			InitializeAnimation(AnimationType.Walk, AnimationDirection.Left);
		}

		[ContextMenu("Lateral Right")]
		public void LateralRight()
		{
			InitializeAnimation(m_currentType, AnimationDirection.Right);
		}*/
	}

}