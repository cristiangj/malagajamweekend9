﻿using System.Collections;
using System.Collections.Generic;
using DataLayer.Variables;
using UnityEngine;

public class CameraMotor : MonoBehaviour {

	public DL_Pose playerPose;

	public float boundX = 2f;
	public float boundY = 2f;
	public float speed = 0.15f;

	private Vector3 desiredPosition;
	private Pose target;

	private void LateUpdate(){
		Vector3 delta = Vector3.zero;
		target = playerPose.GetValue();
		
		float dx = target.position.x - transform.position.x;
		if(Mathf.Abs(dx) > boundX){
			if(transform.position.x < target.position.x){
				delta.x = dx - boundX;
			}else{
				delta.x = dx + boundX;
			}
		}

		float dy = target.position.y - transform.position.y;
		if(Mathf.Abs(dy) > boundY){
			if(transform.position.y < target.position.y){
				delta.y = dy - boundY;
			}else{
				delta.y = dy + boundY;
			}
		}

		desiredPosition = (transform.position + delta);
		transform.position = Vector3.Lerp(transform.position, desiredPosition, speed * Time.deltaTime );
	}
}
