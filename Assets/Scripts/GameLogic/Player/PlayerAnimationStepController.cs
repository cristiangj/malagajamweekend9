﻿using UnityEngine;

namespace GameLogic.Player
{
	public class PlayerAnimationStepController : MonoBehaviour
	{
		public PlayerAnimationController MainController;

		public void Step()
		{
			if (gameObject.activeSelf)
			{
				MainController.Step();
			}
		}
	}
}