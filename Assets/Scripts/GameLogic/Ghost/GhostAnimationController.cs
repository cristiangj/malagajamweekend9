﻿using UnityEngine;

namespace GameLogic.Ghost
{
	public class GhostAnimationController : MonoBehaviour
	{
		public Animator GhostAnimator;
		public GameObject GhostMesh;
		public float GhostVanishAnimationTime;

		private AudioSource m_vanishAudioSource;
		public AudioClip VanishAudioClip;

		private void Awake()
		{
			m_vanishAudioSource = gameObject.AddComponent<AudioSource>();
		}

		public void Show()
		{
			CancelInvoke("HideAnimEnd");
			GhostMesh.SetActive(true);
			GhostAnimator.Play("Idle");
		}

		public void Hide()
		{
			CancelInvoke("HideAnimEnd");
			GhostAnimator.Play("Vanish");

			if (VanishAudioClip == null)
			{
				return;
			}
			if (m_vanishAudioSource.clip != VanishAudioClip)
			{
				m_vanishAudioSource.clip = VanishAudioClip;
				m_vanishAudioSource.loop = false;
			}
			m_vanishAudioSource.Play();


			Invoke("HideAnimEnd", GhostVanishAnimationTime);
		}

		private void HideAnimEnd()
		{
			GhostMesh.SetActive(false);
		}

		private void OnDestroy()
		{
			CancelInvoke("HideAnimEnd");
		}
	}
}