﻿using DataLayer.Variables;
using UnityEngine;

namespace GameLogic.NPC {
	public class Spectrum : MonoBehaviour {

		[Header("Variables")]
		[SerializeField] 
		private DL_Bool _inMovement;
		[SerializeField]
		private DL_Bool _spectrumVisible;

		private void OnEnable() {
			_inMovement.Subscribe(OnPlayerMove);
			_spectrumVisible.SetValue(true);
		}

		private void OnDisable() {
			_inMovement.Unsubscribe(OnPlayerMove);
			_spectrumVisible.SetValue(false);
		}

		private void OnPlayerMove() {
			if (_inMovement.GetValue()) {
				gameObject.SetActive(false);
			}
		}
	}
}