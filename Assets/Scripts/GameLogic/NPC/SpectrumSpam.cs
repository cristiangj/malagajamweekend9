﻿using DataLayer.Variables;
using GameLogic.Ghost;
using UnityEngine;

namespace GameLogic.NPC {
	
	public class SpectrumSpam : MonoBehaviour {

		[SerializeField] 
		private int maxTilesDistance;
		[SerializeField]
		private float _probability = 25;
		[SerializeField]
		private GameObject _spectrumPrefab;
		[SerializeField]
		private LayerMask _layers;
		
		[Header("Variables")] 
		[SerializeField] 
		private DL_Pose _currentPlayerPose;
		[SerializeField]
		private DL_Void _onWallTouch;
		[SerializeField]
		private DL_Vector2 _directionPJ;

		private GameObject _spectrumInstance;
		
		private void Awake() {
			_spectrumInstance = Instantiate(_spectrumPrefab);
			_spectrumInstance.SetActive(false);
			
			_onWallTouch.Subscribe(OnWallTouch);
		}

		private void OnDestroy() {
			_onWallTouch.Unsubscribe(OnWallTouch);
		}

		private void OnWallTouch() {

			//don't respawn again if it is on the scene
			if (_spectrumInstance.activeSelf) {
				return;
			}
			
			float value = Random.Range(1f, 100f);

			if (value <= _probability) {
				Spawn();
			}
		}

		private void Spawn() {

			Vector2 newDirection = GetNewDirection();
			Vector2 directionPJ = _directionPJ.GetValue();

			while (newDirection == directionPJ || IsSomethingInDirection(newDirection)) {
				newDirection = GetNewDirection();
			}

			_spectrumInstance.transform.position = (Vector2) _currentPlayerPose.GetValue().position + newDirection * maxTilesDistance;
			_spectrumInstance.SetActive(true);
			_spectrumInstance.gameObject.GetComponentInChildren<GhostAnimationController>().Show();
		}

		private Vector2 GetNewDirection() {
			int value = Random.Range(-1, 2); //[-1,1]

			while (value == 0) {
				value = Random.Range(-1, 2);
			}
			
			int coordinate = Random.Range(0, 2); //0 = x, 1 = y
			Vector2 vector = new Vector2();
			
			if (coordinate == 0) {
				vector.x = value;
			}
			else {
				vector.y = value;
			}

			return vector;
		}

		private bool IsSomethingInDirection(Vector2 direction) {
			RaycastHit2D hit = Physics2D.Raycast(_currentPlayerPose.GetValue().position, direction, maxTilesDistance, _layers);

			return hit.collider != null;
		}
	}
}
