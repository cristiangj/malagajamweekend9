﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScenes : MonoBehaviour{
    public void LoadA(string scenename){
        SceneManager.LoadScene(scenename);
    }
}