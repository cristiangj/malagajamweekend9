﻿using UnityEngine;

namespace DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Variables/String")]
	public class DL_String : global::DataLayer.Base.DL_DataLayerBase<string> {
		
	}
}
