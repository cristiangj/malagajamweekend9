﻿using UnityEngine;

namespace DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Variables/Transform")]
	public class DL_Transform : global::DataLayer.Base.DL_DataLayerBase<Transform> {
		
	}
}
