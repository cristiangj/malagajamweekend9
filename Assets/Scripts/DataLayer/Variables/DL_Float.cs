﻿using UnityEngine;

namespace DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Variables/Float")]
	public class DL_Float : global::DataLayer.Base.DL_DataLayerBase<float> {
		
	}
}
