﻿using UnityEngine;

namespace DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Variables/Int")]
	public class DL_Int : global::DataLayer.Base.DL_DataLayerBase<int> {
		
	}
}
