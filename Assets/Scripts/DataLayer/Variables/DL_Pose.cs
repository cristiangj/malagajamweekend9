﻿using UnityEngine;

namespace DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Variables/Pose")]
	public class DL_Pose : global::DataLayer.Base.DL_DataLayerBase<Pose> {
		
	}
}
