﻿using UnityEngine;

namespace DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Variables/Bool")]
	public class DL_Bool : global::DataLayer.Base.DL_DataLayerBase<bool> {
		
	}
}
