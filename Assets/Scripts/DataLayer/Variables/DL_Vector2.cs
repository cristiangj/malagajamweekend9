﻿using UnityEngine;

namespace DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Variables/Vector2")]
	public class DL_Vector2 : global::DataLayer.Base.DL_DataLayerBase<Vector2> {
		
	}
}
