﻿
using UnityEngine;

namespace DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Variables/Object")]
	public class DL_Object : global::DataLayer.Base.DL_DataLayerBase<object> {
		
	}
}
