﻿using UnityEngine;

namespace DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Variables/GameObject")]
	public class DL_GameObject: global::DataLayer.Base.DL_DataLayerBase<GameObject> {
	
	}
}
