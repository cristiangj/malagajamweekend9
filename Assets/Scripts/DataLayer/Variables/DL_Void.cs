﻿using UnityEngine;

namespace DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Variables/Void")]
	public class DL_Void : global::DataLayer.Base.DL_DataLayerBase {
		
	}
}
