﻿using System;
using System.ComponentModel;
using UnityEngine;

namespace DataLayer.Base {
	
	/// <summary>
	/// This class is only for raise events without values
	/// </summary>
	public class DL_DataLayerBase : ScriptableObject, DL_IBase {
		
		private event Action _actionEvent;
		
		public void Trigger() {
			if (_actionEvent != null) _actionEvent();
		}

		public void Subscribe(Action callback) {
			_actionEvent += callback;
		}

		public void Unsubscribe(Action callback) {
			_actionEvent -= callback;
		}
		
	}

	/// <summary>
	/// This class stores any value and raises an event when this value change, optionally the invoke function
	/// can be called to raise an event without change the value stored
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class DL_DataLayerBase<T> : ScriptableObject, DL_IBase<T> {
		public T value;

		protected T _oldValue;

		protected event Action _actionEvent;

		//public HideFlags flagsOnEnable;


		public virtual void SetValue(T newValue) {
			value = newValue;
			Trigger();
		}

		public virtual T GetValue() {
			return value;
		}

		public void Trigger() {
			if (_actionEvent != null) _actionEvent();
		}

		public void Subscribe(Action callback) {
			_actionEvent += callback;
		}

		public void Unsubscribe(Action callback) {
			_actionEvent -= callback;
		}

		protected virtual void OnValidate() {
			if (Application.isPlaying) {
				//for debug purposes
				Trigger();
				return;
			}

			_oldValue = value;
		}

		protected virtual void OnDisable() {
			value = _oldValue;
			DeleteCallbacks();
		}

		public virtual void DeleteCallbacks() {
			_actionEvent = null;
		}
	}
}