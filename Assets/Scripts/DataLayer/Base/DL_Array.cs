﻿namespace DataLayer.Base {

	public class DL_Array<T> : DL_DataLayerBase<T[]> {

		public override void SetValue(T[] value) {
			this.value = value;
			Trigger();
		}
	}

}
