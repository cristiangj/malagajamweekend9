﻿using System;
using System.ComponentModel;
using UnityEngine;

namespace DataLayer.Base {
	
	/// <summary>
	/// This interface is only for raise events without values
	/// </summary>
	public interface DL_IBase {

		void Trigger();

		void Subscribe(Action callback);
	
		void Unsubscribe(Action callback);	
	}

	/// <summary>
	/// This interface is for store any value and raises an event when this value change, optionally the invoke function
	/// can be called to raise an event without change the value stored
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface DL_IBase<T> {

		void SetValue(T newValue);

		T GetValue();

		void Trigger();

		void Subscribe(Action callback);

		void Unsubscribe(Action callback);

		void DeleteCallbacks();
	}
}