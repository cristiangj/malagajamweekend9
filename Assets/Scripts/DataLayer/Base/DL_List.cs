﻿using System.Collections.Generic;
using UnityEngine;

namespace DataLayer.Base {

	public class DL_List<T> : DL_DataLayerBase<List<T>> {

		private int count;
		
		public override void SetValue(List<T> value) {
			this.value = value;
			Trigger();
		}

		public void AddValue(T newValue) {
			value.Add(newValue);
			Trigger();
		}
		
		public void RemoveValue(T newValue) {
			value.Remove(newValue);
			Trigger();
		}
		
		protected override void OnValidate() {
			if (Application.isPlaying) {
				//for debug purposes
				Trigger();
				return;
			}

			count = value.Count;
		}

		protected override void OnDisable() {
			value.RemoveRange(count, value.Count - count);
			DeleteCallbacks();
		}
	}

}
