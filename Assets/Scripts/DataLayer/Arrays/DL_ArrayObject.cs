﻿using UnityEngine;

namespace DataLayer.Lists { 

	[CreateAssetMenu( menuName = "DataLayer/Arrays/Array Object")]
	public class DL_ArrayObject : global::DataLayer.Base.DL_Array<object> {
	
	}

}
