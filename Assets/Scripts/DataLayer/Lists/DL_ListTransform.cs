﻿using UnityEngine;

namespace DataLayer.Lists { 

	[CreateAssetMenu( menuName = "DataLayer/Lists/List Transform")]
	public class DL_ListTransform : global::DataLayer.Base.DL_List<Transform> {
	
	}

}
