﻿using System.Collections.Generic;
using UnityEngine;

namespace DataLayer.Lists { 

	[CreateAssetMenu( menuName = "DataLayer/Lists/List GameObject")]
	public class DL_ListGameObject : global::DataLayer.Base.DL_List<GameObject> {
	}

}
