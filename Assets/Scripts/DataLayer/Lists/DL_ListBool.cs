﻿using UnityEngine;

namespace DataLayer.Lists { 

	[CreateAssetMenu( menuName = "DataLayer/Lists/List Bool")]
	public class DL_ListBool : global::DataLayer.Base.DL_List<bool> {
	
	}

}
