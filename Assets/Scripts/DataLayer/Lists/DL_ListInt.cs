﻿using UnityEngine;

namespace DataLayer.Lists { 

	[CreateAssetMenu( menuName = "DataLayer/Lists/List Int")]
	public class DL_ListInt : global::DataLayer.Base.DL_List<int> {
	
	}

}
