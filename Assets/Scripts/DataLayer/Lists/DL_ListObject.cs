﻿using UnityEngine;

namespace DataLayer.Lists { 

	[CreateAssetMenu( menuName = "DataLayer/Lists/List Object")]
	public class DL_ListObject : global::DataLayer.Base.DL_List<object> {
	
	}

}
