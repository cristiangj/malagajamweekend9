﻿using UnityEngine;

namespace DataLayer.Lists {

	[CreateAssetMenu(menuName = "DataLayer/Lists/List Float")]
	public class DL_ListFloat : global::DataLayer.Base.DL_List<float> {

	}
}
