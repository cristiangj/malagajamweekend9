﻿using UnityEngine;

namespace DataLayer.Lists { 

	[CreateAssetMenu( menuName = "DataLayer/Lists/List String")]
	public class DL_ListString : global::DataLayer.Base.DL_List<string> {
	
	}

}
