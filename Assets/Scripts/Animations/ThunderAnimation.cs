﻿using UnityEngine;

public class ThunderAnimation : MonoBehaviour
{
	public float TimeMin;
	public float TimeMax;

	private Animator m_animator;
	private AudioSource m_audioSource1;
	private AudioSource m_audioSource2;

	public void Awake()
	{
		m_animator = GetComponent<Animator>();
		AudioSource[] audios = FindObjectsOfType<AudioSource>();
		for(int i=0; i<audios.Length; i++)
		{
			if(audios[i].gameObject.name == "Thunder1")
			{
				m_audioSource1 = audios[i];
			}
			if (audios[i].gameObject.name == "Thunder2")
			{
				m_audioSource2 = audios[i];
			}
		}
		Invoke("LaunchThunder", Random.Range(TimeMin/3f, TimeMax/3f));
	}

	public void LaunchThunder()
	{
		m_animator.Play("Thunder");
		if(Random.value >= 0.5f)
		{
			m_audioSource1.Play();
		}
		else
		{
			m_audioSource2.Play();
		}
		Invoke("LaunchThunder", Random.Range(TimeMin, TimeMax));
	}

	private void OnDestroy()
	{
		CancelInvoke("LaunchThunder");
	}
}
