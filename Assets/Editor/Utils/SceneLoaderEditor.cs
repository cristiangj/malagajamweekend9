﻿using GameLogic.Utils;
using UnityEditor;

namespace Editor.Managers {
	
	/// <summary>
	/// Custom inspector for the SceneLoader class, this custom inspector allows load an scene from editor
	/// </summary>
	[CustomEditor(typeof(SceneLoader), true)]
	public class SceneLoaderEditor : UnityEditor.Editor
	{
		private SerializedProperty _scenePath;

		void OnEnable() {

			_scenePath = serializedObject.FindProperty("_scenePath");
		}
		
		public override void OnInspectorGUI() {
			
			DrawDefaultInspector();

			SceneAsset oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(_scenePath.stringValue);

			serializedObject.Update();

			EditorGUI.BeginChangeCheck();
			
			SceneAsset newScene = EditorGUILayout.ObjectField("Scene To Load", oldScene, typeof(SceneAsset), false) as SceneAsset;

			if (EditorGUI.EndChangeCheck())
			{
				var newPath = AssetDatabase.GetAssetPath(newScene);
				_scenePath.stringValue = newPath;
			}
			
			serializedObject.ApplyModifiedProperties();
		}
	}
}
